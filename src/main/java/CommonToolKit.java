import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.parseInt;

public class CommonToolKit {
    /**
     * Converts Sweden number format to English number format
     *
     * @param number
     * @return String
     */
    public String convertToEnglishFormat(String number) {

        String newNumber = number.replace(",", ".");
        try {
            Float.parseFloat(newNumber);
            return newNumber;
        } catch (NumberFormatException e) {
            throw e;
        }
    }

    /**
     * Gives the next dose time
     *
     * @param previousDoseTime,duration
     * @return String
     */
    public String getNextDoesTime(Date previousDoseTime, String duration) {
        try {
            int durationInt = parseInt(duration);

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            TimeZone timezone = TimeZone.getTimeZone("Europe/Stockholm");
            Date doseTime = new Date(previousDoseTime.getTime() + TimeUnit.HOURS.toMillis(durationInt));

            if (timezone.inDaylightTime(doseTime)) {
                Date nextDoseTime = new Date(doseTime.getTime() + TimeUnit.HOURS.toMillis(1));
                return sdf.format(nextDoseTime);
            } else if (timezone.inDaylightTime(previousDoseTime) && !timezone.inDaylightTime(doseTime)) {
                Date nextDoseTime = new Date(doseTime.getTime() - TimeUnit.HOURS.toMillis(1));
                return sdf.format(nextDoseTime);
            } else
                return sdf.format(doseTime);
        } catch (NumberFormatException e) {
            throw e;
        }
    }

    /**
     * Converts text with uni codes to Swedish text
     *
     * @param textWithUnicode
     * @return String
     */
    public String getSwedishText(String textWithUnicode) {

        StringBuffer textBuffer = new StringBuffer();
        textBuffer.append(textWithUnicode);
        return String.valueOf(textBuffer);
    }


}
