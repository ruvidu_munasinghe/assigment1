import org.testng.annotations.Test;

import java.util.Date;

import static org.testng.Assert.*;

public class CommonToolKitTest {
    @Test
    void convertToEnglishFormatTest_validInput() {
        CommonToolKit commonToolKit = new CommonToolKit();
        assertEquals("5.6", commonToolKit.convertToEnglishFormat("5,6"));
    }

    @Test
    void convertToEnglishFormatTest_invalidInput() {
        CommonToolKit commonToolKit = new CommonToolKit();
        assertThrows(NumberFormatException.class,
                () -> {
                    commonToolKit.convertToEnglishFormat("abc");
                });
    }

    @Test
    void getNextDoesTimeTest_dayLightTimeInput() {
        CommonToolKit commonToolKit = new CommonToolKit();
        assertEquals("12:35", commonToolKit.getNextDoesTime(new Date(2022, 06, 01, 05, 35), "6"));
    }

    @Test
    void getNextDoesTimeTest_invalidInput() {
        CommonToolKit commonToolKit = new CommonToolKit();
        assertThrows(NumberFormatException.class,
                () -> {
                    commonToolKit.getNextDoesTime(new Date(2022, 06, 01, 05, 35), "abc");
                });
    }

    @Test
    void getNextDoesTimeTest_normalTimeInput() {
        CommonToolKit commonToolKit = new CommonToolKit();
        assertEquals("11:35", commonToolKit.getNextDoesTime(new Date(2022, 01, 01, 05, 35), "6"));
    }

    @Test
    void getSwedishTextTest_validInput() {
        CommonToolKit commonToolKit = new CommonToolKit();
        assertEquals("ȧskȧda", commonToolKit.getSwedishText("\u0227sk\u0227da"));
    }

    @Test
    void getSwedishTextTest_inputWithoutUnicode() {
        CommonToolKit commonToolKit = new CommonToolKit();
        assertEquals("ssskssda", commonToolKit.getSwedishText("ssskssda"));
    }


}